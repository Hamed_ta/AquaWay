//============================================================================//
//         AQua Way Project by Hamed Tahery (Hamed_t@icloud.com)              //
//                          June 6 2018                                       //
//============================================================================//
#include <Arduino.h>
#include <Wire.h>
#include <U8g2lib.h>
#include <RtcDS3231.h>
#include <TimerOne.h>
#include <Rotary.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROM.h>

#include "utils.h"
#include "configs.h"
//============================================================================//
//============================================================================//
RtcDS3231<TwoWire>                  Rtc(Wire);
U8G2_SSD1306_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0,U8X8_PIN_NONE);
Rotary r =                          Rotary(2, 3);
OneWire                             oneWire(PIN_TEMP);
DallasTemperature                   sensors(&oneWire);
//============================================================================//
//============================================================================//
void setupPins()
{
  // output
  pinMode(PIN_RED_LED       , OUTPUT);
  pinMode(PIN_GREEN_LED     , OUTPUT);
  pinMode(PIN_FEED          , OUTPUT);
  pinMode(PIN_MAIN_LIGHT    , OUTPUT);
  pinMode(PIN_CO2           , OUTPUT);
  pinMode(PIN_NIGHT_LIGHT   , OUTPUT);
  pinMode(PIN_FAN           , OUTPUT);
  pinMode(PIN_PUMP          , OUTPUT);
  pinMode(PIN_HEATER        , OUTPUT);
  pinMode(PIN_BUZZ          , OUTPUT);
  // input
  pinMode(PIN_WATER_LEVEL   , INPUT);
  pinMode(PIN_WATER_SALITY  , INPUT);
  pinMode(PIN_TEMP          , INPUT);
  pinMode(PIN_SW_S2         , INPUT);
  pinMode(PIN_SW_S1         , INPUT);
  pinMode(PIN_SW_PRESS      , INPUT);

  digitalWrite(PIN_SW_S1    , HIGH);
  digitalWrite(PIN_SW_S2    , HIGH);
  digitalWrite(PIN_SW_PRESS , HIGH);

  switchRedLED(false);
  switchHeater(false);
  switchPump(false);
  switchCO2(false);
  switchFeed(false);
  switchMainLight(false);

  //Serial.begin(57600);
}
//============================================================================//
//============================================================================//
void setupTimers()
{
  Timer1.initialize(1000000);
}
//============================================================================//
//============================================================================//
void setupPWM()
{
  TCCR0A = 0;           // undo the configuration done by...
  TCCR0B = 0;           // ...the Arduino core library
  TCNT0  = 0;           // reset timer

  TCCR0A    |= _BV(COM0A1)  // non-inverted PWM on ch. A
  |  _BV(COM0B1)   // same on ch; B
  |  _BV(WGM00)
  |  _BV(WGM01);   // mode 10: ph. correct PWM, TOP = ICR1

  TCCR0B    |=   _BV(CS01) |  _BV(CS00)  ;//clkI/O/64 (From prescaler) ;

  setFanVal(0);
  setNightLightVal(0);
}
//============================================================================//
//============================================================================//
void switchGreenLED   (bool state)
{
  digitalWrite(PIN_GREEN_LED, state);
}
//============================================================================//
void switchRedLED     (bool state)
{
  digitalWrite(PIN_RED_LED, state);
}
//============================================================================//
void switchHeater     (bool state)
{
  digitalWrite(PIN_HEATER  , !state);
}
//============================================================================//
void switchPump       (bool state)
{
  digitalWrite(PIN_PUMP , !state);
}
//============================================================================//
void switchCO2        (bool state)
{
  digitalWrite(PIN_CO2  , state);
}
//============================================================================//
void switchMainLight  (bool state)
{
  digitalWrite(PIN_MAIN_LIGHT  , !state);
}
//============================================================================//
void setNightLightVal (uint8_t value )
{
  OCR0A = (255 - (value+155));
}
//============================================================================//
void setFanVal        (uint8_t value )
{
  OCR0B = (255 - (value+155));
}
//============================================================================//
void Buzz       (uint8_t type)
{
  switch (type)
  {
    case 1:
    digitalWrite(PIN_BUZZ,HIGH);
    delay(80);
    digitalWrite(PIN_BUZZ,LOW);
    delay(80);
    digitalWrite(PIN_BUZZ,HIGH);
    delay(250);
    digitalWrite(PIN_BUZZ,LOW);
    break;
    case 2:
    digitalWrite(PIN_BUZZ,HIGH);
    delay(80);
    digitalWrite(PIN_BUZZ,LOW);
    break;
    case 3:
    digitalWrite(PIN_BUZZ,HIGH);
    delay(20);
    digitalWrite(PIN_BUZZ,LOW);
    break;
    case 4:
    digitalWrite(PIN_BUZZ,HIGH);
    delay(40);
    digitalWrite(PIN_BUZZ,LOW);
    delay(40);
    digitalWrite(PIN_BUZZ,HIGH);
    delay(40);
    digitalWrite(PIN_BUZZ,LOW);
    delay(40);
    digitalWrite(PIN_BUZZ,HIGH);
    delay(40);
    digitalWrite(PIN_BUZZ,LOW);
    delay(40);
    digitalWrite(PIN_BUZZ,HIGH);
    delay(40);
    digitalWrite(PIN_BUZZ,LOW);
    break;
    case 5:
    digitalWrite(PIN_BUZZ,HIGH);
    delay(100);
    digitalWrite(PIN_BUZZ,LOW);
    delay(100);
    digitalWrite(PIN_BUZZ,HIGH);
    delay(100);
    digitalWrite(PIN_BUZZ,LOW);
    delay(100);
    digitalWrite(PIN_BUZZ,HIGH);
    delay(100);
    digitalWrite(PIN_BUZZ,LOW);
    delay(100);
    digitalWrite(PIN_BUZZ,HIGH);
    delay(100);
    digitalWrite(PIN_BUZZ,LOW);
    break;
  }
}
//============================================================================//
void switchFeed       (bool state)
{
  digitalWrite(PIN_FEED  , !state);
}
//============================================================================//
U8G2_SSD1306_128X64_NONAME_1_HW_I2C *setupDisplay()
{
  u8g2.begin();
  return &u8g2;
}
//============================================================================//
void setupRTC()
{
  Rtc.Begin();
  //Rtc.GetDateTime();
  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
  if (!Rtc.IsDateTimeValid())
  {
    //Serial.println("RTC lost confidence in the DateTime!");
    Rtc.SetDateTime(compiled);
  }

  if (!Rtc.GetIsRunning())
  {
    //Serial.println("RTC was not actively running, starting now");
    Rtc.SetIsRunning(true);
  }
  RtcDateTime now =  Rtc.GetDateTime();
  if (now < compiled)
  {
    //Serial.println("RTC is older than compile time!  (Updating DateTime)");
    Rtc.SetDateTime(compiled);
  }
  else if (now > compiled)
  {
    //Serial.println("RTC is newer than compile time. (this is expected)");
  }
  else if (now == compiled)
  {
    //Serial.println("RTC is the same as compile time! (not expected but all is fine)");
  }
  //Rtc.Enable32kHzPin(false);
  //Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);
}
//============================================================================//
void rtcGetTime(uint32_t &ts,uint8_t &h,uint8_t &m,uint8_t &s)
{
  RtcDateTime now = Rtc.GetDateTime();
  h  = now.Hour();
  m  = now.Minute();
  s  = now.Second();
  ts = now.TotalSeconds();
}
//============================================================================//
Rotary *setupRotary()
{
  PCICR  |= (1 << PCIE2);
  PCMSK2 |= (1 << PCINT18) | (1 << PCINT19);

  return &r;
}
//============================================================================//
void setupSensor()
{
  sensors.begin();
  //Serial.print("Found ");
  //Serial.print(sensors.getDeviceCount(), DEC);
  //Serial.println(" devices.");
}
//============================================================================//
float getWaterTemp()
{
  DeviceAddress  outsideThermometer;
  if (!sensors.getAddress(outsideThermometer, 1))
  {
    //Serial.println("Unable to find address for Device 0");
    return 0;
  }
  sensors.requestTemperatures();
  return sensors.getTempC(outsideThermometer);
}
//============================================================================//
float getAirTemp()
{
  DeviceAddress  insideThermometer;
  if (!sensors.getAddress(insideThermometer, 0))
  {
    //Serial.println("Unable to find address for Device 1");
    return 0;
  }
  //sensors.requestTemperatures();
  return sensors.getTempC(insideThermometer);
}
//============================================================================//
uint8_t   getWaterLevel()
{
  return analogRead(PIN_WATER_LEVEL);
}
//============================================================================//
uint8_t   getWaterSality()
{
  return analogRead(PIN_WATER_SALITY);
}
//============================================================================//
uint8_t   getFanLevel()
{
  return (100 - OCR0B);
}
//============================================================================//
uint8_t   getnLightLevel()
{
  return (100 - OCR0A);
}
//============================================================================//
uint8_t   getHeaterState()
{
  return !digitalRead(PIN_HEATER);
}
//============================================================================//
uint8_t   getCO2State()
{
  return digitalRead(PIN_CO2);
}
//============================================================================//
uint8_t   getLightState()
{
  return !digitalRead(PIN_MAIN_LIGHT);
}
//============================================================================//
uint8_t   getFeedState()
{
  return !digitalRead(PIN_FEED);
}
//============================================================================//
uint8_t   getPumpState()
{
  return !digitalRead(PIN_PUMP);
}
//============================================================================//
void EEPROM_writeDouble(int ee, double value)
{
  byte* p = (byte*)(void*)&value;
  for (uint8_t i = 0; i < sizeof(value); i++)
     EEPROM.write(ee++, *p++);
}
//============================================================================//
double EEPROM_readDouble(int ee)
{
  double value = 0.0;
  byte* p = (byte*)(void*)&value;
  for (uint8_t i = 0; i < sizeof(value); i++)
      *p++ = EEPROM.read(ee++);
  return value;
}
//============================================================================//
uint32_t EEPROM_readLong(int ee)
{
  uint32_t value = 0;
  byte* p = (byte*)(void*)&value;
  for (uint8_t i = 0; i < sizeof(value); i++)
      *p++ = EEPROM.read(ee++);
  return value;
}
//============================================================================//
void EEPROM_writeLong(int ee, uint32_t value)
{
  byte* p = (byte*)(void*)&value;
  for (uint8_t i = 0; i < sizeof(value); i++)
      EEPROM.write(ee++, *p++);
}
//============================================================================//
