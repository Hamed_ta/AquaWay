#pragma once
//============================================================================//
#include <Arduino.h>
//============================================================================//
#define PIN_RED_LED      A1
#define PIN_TEMP         A0
#define PIN_CO2          A2
#define PIN_WATER_LEVEL  A7
#define PIN_WATER_SALITY A6

#define PIN_GREEN_LED    12
#define PIN_MAIN_LIGHT   11
#define PIN_FEED         4
#define PIN_NIGHT_LIGHT  5
#define PIN_FAN          6
#define PIN_PUMP         7
#define PIN_HEATER       8
#define PIN_BUZZ         10
#define PIN_SW_S2        3
#define PIN_SW_S1        2
#define PIN_SW_PRESS     13
//============================================================================//
