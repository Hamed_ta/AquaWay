#pragma once
//============================================================================/
#include <U8g2lib.h>
#include <Wire.h>
#include <RtcDS3231.h>
#include <Rotary.h>
#include <DallasTemperature.h>
//============================================================================//
void      setupPins();
void      setupTimers();
void      setupPWM();
void      setupSensor();
void      setupRTC();
void      rtcGetTime(uint32_t &ts,uint8_t &h,uint8_t &m,uint8_t &s);
float     getWaterTemp();
float     getAirTemp();
uint8_t   getWaterLevel();
uint8_t   getWaterSality();
uint8_t   getFanLevel();
uint8_t   getnLightLevel();
uint8_t   getHeaterState();
uint8_t   getCO2State();
uint8_t   getLightState();
uint8_t   getFeedState();
uint8_t   getPumpState();
double EEPROM_readDouble(int ee);
void EEPROM_writeDouble(int ee, double value);
uint32_t EEPROM_readLong(int ee);
void EEPROM_writeLong(int ee, uint32_t value);
//============================================================================//
U8G2_SSD1306_128X64_NONAME_1_HW_I2C *setupDisplay();
Rotary                              *setupRotary();
//============================================================================//
void switchGreenLED   (bool state);
void switchRedLED     (bool state);
void switchHeater     (bool state);
void switchPump       (bool state);
void switchCO2        (bool state);
void switchMainLight  (bool state);
void setNightLightVal (uint8_t value );
void setFanVal        (uint8_t value );
void Buzz             (uint8_t type);
void switchFeed       (bool state);
//============================================================================//
//============================================================================//
