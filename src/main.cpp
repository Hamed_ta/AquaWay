//============================================================================//
//         AQua Way Project by Hamed Tahery (Hamed_t@icloud.com)              //
//                          June 6 2018                                       //
//============================================================================//
#include <Arduino.h>
#include <TimerOne.h>
#include <Rotary.h>
#include <OneWire.h>
#include <Wire.h>
#include <U8g2lib.h>
#include <DallasTemperature.h>
#include <EEPROM.h>
//-----------------------------------------------------------------------------
#include "utils.h"
#include "configs.h"
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void timerIsr();
void drawData();
void readData();
void procData();
void loadData();
void saveData();
void saveTolalSecs();
void loadTotalSecs();
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
U8G2_SSD1306_128X64_NONAME_1_HW_I2C *u8g2P;
Rotary                              *rotary;
//-----------------------------------------------------------------------------
float   waterTemp  , airTemp , setTemp;
uint8_t waterLevel , waterSanity ,LastOH;
uint8_t hour,minute,second;
uint32_t tcSeconds;
volatile uint32_t tnSeconds;
volatile uint8_t fanLevel   ,nLightLevel,heaterState,CO2State,LightState,feedState,pumpState;
volatile uint8_t SetNLightLevel,SetNLightOffLevel,SetOnLight,SetOffLight,SetOnCO2,SetOffCO2,SetOffNLight,SetOnPump,SetOffPump,waterLevelFeedOn;
volatile float DayPassed;
volatile bool readFlag,procFlag,MenuFlag,inMenu,dimFlag,fulFlag;
volatile uint8_t procIndex,menuFlagIndex,menuTimeOut,pageIndex,dimTimeOut;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void setup()
{
  //========================================================
  setupPins   ();
  setupPWM    ();
  setupRTC    ();
  u8g2P         = setupDisplay();
  rotary        = setupRotary ();
  setupSensor();
  setupTimers (); Timer1.attachInterrupt(timerIsr);
  loadData();
  Buzz(1);
  pageIndex = 1;
  //========================================================
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void loop()
{
  if (!inMenu)
  {
    if (readFlag)
    {
      readFlag = false;
      readData();
    }else
    if (procFlag)
    {
      procFlag = false;
      procData();
    }

    if (MenuFlag)
    {
      readData();
      loadData();
      MenuFlag = false;
      Buzz(2);
      inMenu = true;
      menuFlagIndex = 1;
    }

    if (dimFlag)
    {
      dimFlag = false;
      u8g2P->setContrast(0);
    }

    if (fulFlag)
    {
      fulFlag = false;
      u8g2P->setContrast(255);
    }

  } else
  {
    if (menuFlagIndex > 18)
    {
      saveData();
      Buzz(2);
      inMenu = false;
    }
  }
  //|||||||||||||||||||||||||||||||||||
  u8g2P->firstPage();
  do
  {
    drawData();
  } while (u8g2P->nextPage());
  //|||||||||||||||||||||||||||||||||||
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void readData()
{
  switchGreenLED       (true);
  rtcGetTime           (tcSeconds,hour,minute,second);
  waterLevel         = (getWaterLevel()   / 255.0) * 100;
  waterSanity        = (getWaterSality()  / 255.0) * 100;
  waterTemp          = getWaterTemp();
  airTemp            = getAirTemp();
  fanLevel           = getFanLevel() ;
  nLightLevel        = getnLightLevel();
  heaterState        = getHeaterState();
  CO2State           = getCO2State();
  LightState         = getLightState();
  feedState          = getFeedState();
  pumpState          = getPumpState();
  switchGreenLED       (false);
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void procData()
{
  int16_t compW  = ((waterTemp - setTemp) * 10);
  int16_t compA  = ((airTemp   - setTemp) * 3 );

  //Serial.print(compW);
  //Serial.println(" CompW");
  //Serial.print(compA);
  //Serial.println(" CompA");

  if (compA < -30)
      compA = -30;

  if (compA > 30)
      compA = 30;

  if (compW < 0)
  {
    setFanVal(0);
    if (compW < -2)
       switchHeater(true);
    if (compW < -60)
       Buzz(5);
  }
  else
  {
    switchHeater(false);
    if (compW > 60)
        Buzz(5);

    if (compW > 100)
        compW = 100;
  }

  int8_t comp   = compW + compA;

  if (comp > 100)
      comp = 100;

  if (comp < 0)
      comp = 0;

  setFanVal(comp);

  //Serial.print(comp);
  //Serial.println(" Comp");
  //.......................................
  switchMainLight ((hour >= SetOnLight) && (hour < SetOffLight));
  //.......................................
  switchCO2       ((hour >= SetOnCO2)   && (hour < SetOffCO2));
  //.......................................
  if ((hour == SetOnPump))
      switchPump      (true);
  else
  if ((hour == SetOffPump))
      switchPump      (false);
  //.......................................
  switchRedLED((waterLevel < (waterLevelFeedOn / 2)) || (compW < -50) || (compW > 50) || (DayPassed > 10));
  //.......................................
  if (waterLevel >= waterLevelFeedOn)
  {
    switchFeed(true);
  }
  else
  {
    switchFeed(false);
    if (waterLevel < (waterLevelFeedOn / 2))
        Buzz(4);
  }
  //.......................................
  if ((hour >= SetOffLight) && (hour < SetOffNLight))
  {
    setNightLightVal(SetNLightLevel);
  } else
  if (hour >= SetOffNLight)
  {
    setNightLightVal(SetNLightOffLevel);
  }
  if ((hour >= SetOnLight) && (hour < SetOffLight))
  {
    setNightLightVal(0);
  }
  //.......................................
  uint32_t def = (tcSeconds - tnSeconds);
  DayPassed = (def / 86400.0);
  if (DayPassed > 20)
      Buzz(5);
  //.......................................
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void drawData()
{
  char data[20];
  char stemp1[8],stemp2[5];
  u8g2P->drawRFrame(0, 0, 128,22,4);
  if (!inMenu)
  {
    switch (pageIndex)
    {
      //8888888888888888888888888888888888888888888888888888888888888888888888
      case 1:
      u8g2P->setFont(u8g2_font_ncenB18_tr);
      u8g2P->drawStr(0, 20, "~~TIME~~");
      u8g2P->setFont(u8g2_font_ncenR14_tr);
      sprintf(data,"%02u:%02u:%02u",hour,minute,second);
      u8g2P->drawStr(30, 40,data);
      u8g2P->drawRFrame(0, 23, 128,20,4);
      u8g2P->setFont(u8g2_font_ncenR10_tr );
      dtostrf(DayPassed, 4, 2, stemp1);
      sprintf(data,"Day Passed :%5s",stemp1);
      u8g2P->drawStr(5, 58,data);
      u8g2P->drawRFrame(0, 44, 128,19,4);
      break;
      //8888888888888888888888888888888888888888888888888888888888888888888888
      case 2:
      dtostrf(waterTemp, 3, 1, stemp1);
      dtostrf(airTemp  , 3, 1, stemp2);
      u8g2P->setFont(u8g2_font_ncenB18_tr);
      u8g2P->drawStr(11, 20, "~TEMP~");
      u8g2P->setFont(u8g2_font_ncenR14_tr);
      sprintf(data,"Water : %s °C",stemp1);
      u8g2P->drawStr(5, 41,data);
      sprintf(data,"Air      : %s °C",stemp2);
      u8g2P->drawStr(5, 61,data);
      u8g2P->drawRFrame(0, 23, 128,41,4);
      break;
      //8888888888888888888888888888888888888888888888888888888888888888888888
      case 3:
      u8g2P->setFont(u8g2_font_ncenB18_tr);
      u8g2P->drawStr(6, 20, "~LEVEL~");
      u8g2P->setFont(u8g2_font_ncenR14_tr);
      u8g2P->drawRFrame(0, 23, 128,41,4);
      sprintf(data,"W:%2d%% S:%2d%%",waterLevel,waterSanity);
      u8g2P->drawStr(5, 41,data);
      sprintf(data,"F :%2d%% L:%2d%%",fanLevel,nLightLevel);
      u8g2P->drawStr(5, 61,data);
      break;
      //8888888888888888888888888888888888888888888888888888888888888888888888
      case 4:
      u8g2P->setFont(u8g2_font_ncenB18_tr);
      u8g2P->drawStr(10, 20, "STATUS");
      u8g2P->setFont(u8g2_font_ncenR14_tr);
      u8g2P->drawRFrame(0, 23, 128,41,4);

      if (heaterState) u8g2P->drawStr   (8, 51,"H");
      u8g2P->drawRFrame(5, 34, 21, 20, 2);

      if (CO2State) u8g2P->drawStr      (33, 51,"C");
      u8g2P->drawRFrame(29, 34, 21, 20, 2);

      if (LightState) u8g2P->drawStr    (58, 51,"L");
      u8g2P->drawRFrame(53, 34, 21, 20, 2);

      if (feedState) u8g2P->drawStr     (83, 51,"F");
      u8g2P->drawRFrame(77, 34, 21, 20, 2);

      if (pumpState) u8g2P->drawStr     (108, 51,"P");
      u8g2P->drawRFrame(102, 34, 21, 20, 2);
      break;
      //8888888888888888888888888888888888888888888888888888888888888888888888
      case 5:
      u8g2P->setFont(u8g2_font_ncenB18_tr);
      u8g2P->drawStr(4, 20, "AUTHOR");
      u8g2P->setFont(u8g2_font_ncenR14_tr);
      u8g2P->setDrawColor(1);
      u8g2P->drawRFrame(0, 23, 128,41,4);
      u8g2P->drawStr(33, 40,"Hamed");
      u8g2P->drawStr(29, 57,"Tahery ;)");
      break;
      //8888888888888888888888888888888888888888888888888888888888888888888888
    }

  } else
  {
    u8g2P->setFont(u8g2_font_ncenB18_tr);
    u8g2P->drawStr(7, 20, "~MENU~");
    u8g2P->drawRFrame(0, 23, 128,41,4);
    u8g2P->setFont(u8g2_font_ncenR10_tr);
    switch (menuFlagIndex)
    {
      case 1:
      u8g2P->drawStr(10, 40, "Set Water Temp");
      dtostrf(setTemp, 3, 1, stemp1);
      sprintf(data,"Value : %s C",stemp1);
      u8g2P->drawStr(20, 58,data);
      break;
      case 2:
      u8g2P->drawStr(7, 40, "Set Night Light L");
      sprintf(data,"Value : %u %%",SetNLightLevel);
      u8g2P->drawStr(24, 58,data);
      break;
      case 3:
      u8g2P->drawStr(7, 40, "Set M Night Li L");
      sprintf(data,"Value : %u %%",SetNLightOffLevel);
      u8g2P->drawStr(24, 58,data);
      break;
      case 4:
      u8g2P->drawStr(7, 40, "Reset Day Passed");
      dtostrf(DayPassed, 4, 2, stemp1);
      sprintf(data,"Day Passed: %4s",stemp1);
      u8g2P->drawStr(9, 58,data);
      break;
      case 5:
      u8g2P->drawStr(8, 40, "Set Water LV On");
      sprintf(data,"Value : %3u st",waterLevelFeedOn);
      u8g2P->drawStr(22, 58,data);
      break;
      case 6:
      u8g2P->drawStr(3, 40, "Set Light On Time");
      sprintf(data,"Time : %2u OC",SetOnLight);
      u8g2P->drawStr(21, 58,data);
      break;
      case 7:
      u8g2P->drawStr(5, 40, "Set Light Of Time");
      sprintf(data,"Time : %2u OC",SetOffLight);
      u8g2P->drawStr(21, 58,data);
      break;
      case 8:
      u8g2P->drawStr(6, 40, "Set CO2 On Time");
      sprintf(data,"Time : %2u OC",SetOnCO2);
      u8g2P->drawStr(21, 58,data);
      break;
      case 9:
      u8g2P->drawStr(6, 40, "Set CO2 Off Time");
      sprintf(data,"Time : %2u OC",SetOffCO2);
      u8g2P->drawStr(21, 58,data);
      break;
      case 10:
      u8g2P->drawStr(12, 40, "Set M Light Off");
      sprintf(data,"Time : %2u OC",SetOffNLight);
      u8g2P->drawStr(21, 58,data);
      break;
      case 11:
      u8g2P->drawStr(5, 40, "Set Pum On Time");
      sprintf(data,"Time : %2u OC",SetOnPump);
      u8g2P->drawStr(21, 58,data);
      break;
      case 12:
      u8g2P->drawStr(6, 40, "Set Pum Of Time");
      sprintf(data,"Time : %2u OC",SetOffPump);
      u8g2P->drawStr(21, 58,data);
      break;
      case 13:
      u8g2P->drawStr(15, 40, "Test Fan Level");
      sprintf(data,"Value : %3u %%",fanLevel);
      u8g2P->drawStr(22, 58,data);
      break;
      case 14:
      u8g2P->drawStr(16, 40, "Test Air Pump");
      sprintf(data,"State : %u ",pumpState);
      u8g2P->drawStr(38, 58,data);
      break;
      case 15:
      u8g2P->drawStr(10, 40, "Test Main Light");
      sprintf(data,"State : %u ",LightState);
      u8g2P->drawStr(38, 58,data);
      break;
      case 16:
      u8g2P->drawStr(26, 40, "Test Heater");
      sprintf(data,"State : %u ",heaterState);
      u8g2P->drawStr(38, 58,data);
      break;
      case 17:
      u8g2P->drawStr(8, 40, "Test Filter Pump");
      sprintf(data,"State : %u ",feedState);
      u8g2P->drawStr(38, 58,data);
      break;
      case 18:
      u8g2P->drawStr(14, 40, "Test CO2 Valve");
      sprintf(data,"State : %u ",CO2State);
      u8g2P->drawStr(38, 58,data);
      break;
    }
  }
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void timerIsr()
{
  procIndex++;
  switch (procIndex)
  {
    case 1:
    readFlag = true;
    break;
    case 2:
    readFlag = true;
    break;
    case 3:
    procFlag = true;
    procIndex = 0;
    break;
  }

  if (!digitalRead(PIN_SW_PRESS))
  {
    if (!inMenu)
    {
      MenuFlag = true;
    } else
    {
      menuFlagIndex++;
      Buzz(3);
    }
  }

  if (inMenu)
  {
    menuTimeOut++;
    if (menuTimeOut >= 120)
    {
      Buzz(3);
      inMenu = false;
      menuTimeOut = 0;
    }
  }

  dimTimeOut++;
  if (dimTimeOut >= 60)
  {
    dimTimeOut = 0;
    dimFlag = true;
    pageIndex++;
    if (pageIndex > 5)
       pageIndex = 1;
  }
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
ISR(PCINT2_vect)
{
  dimTimeOut = 0;

  unsigned char result = rotary->process();
  if (inMenu)
  {
    menuTimeOut = 0;
    if (result == DIR_CW)
    {
      switch (menuFlagIndex)
      {
        case 1:
        setTemp-=0.1;
        if (setTemp < 10)
        setTemp = 10;
        break;
        case 2:
        SetNLightLevel-=1;
        if (SetNLightLevel < 1)
        SetNLightLevel = 1;
        setNightLightVal(SetNLightLevel);
        break;
        case 3:
        SetNLightOffLevel-=1;
        if (SetNLightOffLevel < 1)
        SetNLightOffLevel = 1;
        setNightLightVal(SetNLightOffLevel);
        break;
        case 4:
        DayPassed = 0;
        tnSeconds = tcSeconds;
        break;
        case 5:
        waterLevelFeedOn-=1;
        if (waterLevelFeedOn < 1)
        waterLevelFeedOn = 1;
        break;
        case 6:
        SetOnLight-=1;
        if (SetOnLight < 1)
        SetOnLight = 1;
        break;
        case 7:
        SetOffLight-=1;
        if (SetOffLight < 1)
        SetOffLight = 1;
        break;
        case 8:
        SetOnCO2-=1;
        if (SetOnCO2 < 1)
        SetOnCO2 = 1;
        break;
        case 9:
        SetOffCO2-=1;
        if (SetOffCO2 < 1)
        SetOffCO2 = 1;
        break;
        case 10:
        SetOffNLight-=1;
        if (SetOffNLight < 1)
        SetOffNLight = 1;
        break;
        case 11:
        SetOnPump-=1;
        if (SetOnPump < 1)
        SetOnPump = 1;
        break;
        case 12:
        SetOffPump-=1;
        if (SetOffPump < 1)
        SetOffPump = 1;
        break;
        case 13:
        fanLevel-=1;
        if (fanLevel < 1)
        fanLevel = 1;
        setFanVal(fanLevel);
        break;
        case 14:
        pumpState = !pumpState;
        switchPump(pumpState);
        break;
        case 15:
        LightState = !LightState;
        switchMainLight(LightState);
        break;
        case 16:
        heaterState = !heaterState;
        switchHeater(heaterState);
        break;
        case 17:
        feedState = !feedState;
        switchFeed(feedState);
        break;
        case 18:
        CO2State = ! CO2State;
        switchCO2(CO2State);
        break;
      }
    }
    //--------------------------------------
    else if (result == DIR_CCW)
    {
      switch (menuFlagIndex)
      {
        case 1:
        setTemp+=0.1;
        if (setTemp > 50)
        setTemp = 50;
        break;
        case 2:
        SetNLightLevel+=1;
        if (SetNLightLevel > 100)
        SetNLightLevel = 100;
        setNightLightVal(SetNLightLevel);
        break;
        case 3:
        SetNLightOffLevel+=1;
        if (SetNLightOffLevel > 100)
        SetNLightOffLevel = 100;
        setNightLightVal(SetNLightOffLevel);
        break;
        case 4:
         DayPassed = 0;
         tnSeconds = tcSeconds;
        break;
        case 5:
        waterLevelFeedOn+=1;
        if (waterLevelFeedOn > 255)
        waterLevelFeedOn = 255;
        break;
        case 6:
        SetOnLight+=1;
        if (SetOnLight > 23)
        SetOnLight = 23;
        break;
        case 7:
        SetOffLight+=1;
        if (SetOffLight > 23)
        SetOffLight = 23;
        break;
        case 8:
        SetOnCO2+=1;
        if (SetOnCO2 > 23)
        SetOnCO2 = 23;
        break;
        case 9:
        SetOffCO2+=1;
        if (SetOffCO2 > 23)
        SetOffCO2 = 23;
        break;
        case 10:
        SetOffNLight+=1;
        if (SetOffNLight > 23)
        SetOffNLight = 23;
        break;
        case 11:
        SetOnPump+=1;
        if (SetOnPump > 23)
        SetOnPump = 23;
        break;
        case 12:
        SetOffPump+=1;
        if (SetOffPump > 23)
        SetOffPump = 23;
        break;
        case 13:
        fanLevel+=1;
        if (fanLevel > 100)
        fanLevel = 100;
        setFanVal(fanLevel);
        break;
        case 14:
        pumpState = !pumpState;
        switchPump(pumpState);
        break;
        case 15:
        LightState = !LightState;
        switchMainLight(LightState);
        break;
        case 16:
        heaterState = !heaterState;
        switchHeater(heaterState);
        break;
        case 17:
        feedState = !feedState;
        switchFeed(feedState);
        break;
        case 18:
        CO2State = ! CO2State;
        switchCO2(CO2State);
        break;
      }
    }
  } else
  {
    if (result == DIR_CW)
    {
      pageIndex--;
      if (pageIndex < 1)
      pageIndex = 5;
    }
    else if (result == DIR_CCW)
    {
      pageIndex++;
      if (pageIndex > 5)
      pageIndex = 1;
    }
  }

  fulFlag = true;
  dimTimeOut = 0;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void loadData()
{
  if (EEPROM.read(1) != 1)
  {
    EEPROM.write(1,1);
    setTemp           = 27.1;
    SetNLightLevel    = 50;
    SetOnLight        = 9;
    SetOffLight       = 15;
    SetOnCO2          = 10;
    SetOffCO2         = 14;
    SetOffNLight      = 23;
    SetNLightOffLevel = 20;
    SetOnPump         = 22;
    SetOffPump        = 8;
    DayPassed         = 0;
    waterLevelFeedOn  = 10;
    EEPROM_writeDouble (2,setTemp);
    EEPROM.write       (7,SetNLightLevel);
    EEPROM.write       (9,SetOnLight);
    EEPROM.write       (10,SetOffLight);
    EEPROM.write       (11,SetOnCO2);
    EEPROM.write       (12,SetOffCO2);
    EEPROM.write       (13,SetOffNLight);
    EEPROM.write       (14,SetNLightOffLevel);
    EEPROM.write       (15,SetOnPump);
    EEPROM.write       (16,SetOffPump);
    EEPROM.write       (17,waterLevelFeedOn);
    readData();
    tnSeconds = tcSeconds;
    saveTolalSecs      ();
  }
  setTemp           = EEPROM_readDouble(2);
  SetNLightLevel    = EEPROM.read      (7);
  SetOnLight        = EEPROM.read      (9);
  SetOffLight       = EEPROM.read      (10);
  SetOnCO2          = EEPROM.read      (11);
  SetOffCO2         = EEPROM.read      (12);
  SetOffNLight      = EEPROM.read      (13);
  SetNLightOffLevel = EEPROM.read      (14);
  SetOnPump         = EEPROM.read      (15);
  SetOffPump        = EEPROM.read      (16);
  waterLevelFeedOn  = EEPROM.read      (17);
  loadTotalSecs();
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void saveData()
{
  cli();
  EEPROM_writeDouble (2,setTemp);
  EEPROM.write       (7,SetNLightLevel);
  EEPROM.write       (9,SetOnLight);
  EEPROM.write       (10,SetOffLight);
  EEPROM.write       (11,SetOnCO2);
  EEPROM.write       (12,SetOffCO2);
  EEPROM.write       (13,SetOffNLight);
  EEPROM.write       (14,SetNLightOffLevel);
  EEPROM.write       (15,SetOnPump);
  EEPROM.write       (16,SetOffPump);
  EEPROM.write       (17,waterLevelFeedOn);
  saveTolalSecs      ();
  sei();
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void saveTolalSecs()
{
   EEPROM_writeLong(18,tnSeconds);
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void loadTotalSecs()
{
 tnSeconds = EEPROM_readLong(18);
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
